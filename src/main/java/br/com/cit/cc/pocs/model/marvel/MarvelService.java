package br.com.cit.cc.pocs.model.marvel;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

@Service
public class MarvelService {

	@Inject
	private MarvelGateway marvelGateway;

	private static final String PUBLIC_KEY = "8af57122350603aebf6af49a4ac77e8f";
	private static final String PRIVATE_KEY = "904463aff52298c6cc4cee63f374b5625a2a5119";

	public MarvelResponse characters() throws NoSuchAlgorithmException {

		final long timestamp = System.currentTimeMillis();
		final String hash = generateHash(timestamp, PUBLIC_KEY, PRIVATE_KEY);

		final MarvelRequest request = new MarvelRequest(PUBLIC_KEY, hash, timestamp, "Wolverine");
		final MarvelResponse characters = marvelGateway.characters(request);
		return characters;
	}

	private String generateHash(final Long timestamp, final String publicKey, final String privateKey) throws NoSuchAlgorithmException {

		final String hash = timestamp + privateKey + publicKey;

		final MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(hash.getBytes());
		final byte byteData[] = md.digest();

		final StringBuffer sb = new StringBuffer();
		for (final byte element : byteData) {
			sb.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

}
