package br.com.cit.cc.pocs.model.marvel;

public class MarvelRequest {

	private final String publicKey;

	private final String hash;

	private final Long timestamp;

	private final String name;

	public MarvelRequest(final String publicKey, final String hash, final Long timestamp, final String name) {
		this.publicKey = publicKey;
		this.hash = hash;
		this.timestamp = timestamp;
		this.name = name;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public String getHash() {
		return hash;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public String getName() {
		return name;
	}

}
