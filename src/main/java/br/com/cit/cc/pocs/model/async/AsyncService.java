package br.com.cit.cc.pocs.model.async;

import org.springframework.integration.annotation.Publisher;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {

	// publicando o retorno do metodo no channel “dojo“ que será responsável
	// por enviar o email
	@Publisher(channel = "dojo")
	public AsyncDTO createEmail() {
		return new AsyncDTO("dfceconello@gmail.com", "test text", "test subject");
	}

}
