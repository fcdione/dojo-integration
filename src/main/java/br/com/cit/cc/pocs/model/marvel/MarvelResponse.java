package br.com.cit.cc.pocs.model.marvel;

import java.io.Serializable;

public class MarvelResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5086971056235298648L;

	private Long code;
	private String status;
	private String copyright;
	private String attributionText;
	private String attributionHTML;
	private String etag;

	public Long getCode() {
		return code;
	}

	public void setCode(final Long code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(final String copyright) {
		this.copyright = copyright;
	}

	public String getAttributionText() {
		return attributionText;
	}

	public void setAttributionText(final String attributionText) {
		this.attributionText = attributionText;
	}

	public String getAttributionHTML() {
		return attributionHTML;
	}

	public void setAttributionHTML(final String attributionHTML) {
		this.attributionHTML = attributionHTML;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(final String etag) {
		this.etag = etag;
	}

}
