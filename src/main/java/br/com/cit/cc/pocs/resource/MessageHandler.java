package br.com.cit.cc.pocs.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

import br.com.cit.cc.pocs.model.message.dto.MessageDTO;

public class MessageHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandler.class);

	public void receive(final Message<?> message) throws MessagingException {
		if (MessageDTO.class.isAssignableFrom(message.getPayload().getClass())) {
			final MessageDTO messageDTO = (MessageDTO) message.getPayload();
			System.out.println("receive message: " + messageDTO.getText());
		} else {
			throw new RuntimeException("can't read this message");
		}
	}

	public void proccess(final MessageDTO messageDTO) {
		LOGGER.info("message={}", messageDTO);
	}

}
