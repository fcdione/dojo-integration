package br.com.cit.cc.pocs.test;

import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import br.com.cit.cc.pocs.model.marvel.MarvelResponse;
import br.com.cit.cc.pocs.model.marvel.MarvelService;

public class MarvelIntegrationTest {

	private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(MarvelIntegrationTest.class);

	public static void main(final String[] args) throws NoSuchAlgorithmException {
		final ApplicationContext appContext = new ClassPathXmlApplicationContext("spring-http.xml");
		final MarvelService marvelService = appContext.getBean(MarvelService.class);
		final MarvelResponse characters = marvelService.characters();

		LOGGER.info("response: {}", characters);
	}

}
