package br.com.cit.cc.pocs.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.cit.cc.pocs.model.async.AsyncService;

@Controller
@RequestMapping("async")
public class AsyncController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AsyncController.class);

	@Autowired
	private AsyncService asyncService;

	@RequestMapping(value = "/async", method = RequestMethod.GET)
	@ResponseBody
	String async() {
		LOGGER.info("call async service start ...");
		asyncService.createEmail();
		LOGGER.info("call async service finish ...");
		return "OK";
	}

}
