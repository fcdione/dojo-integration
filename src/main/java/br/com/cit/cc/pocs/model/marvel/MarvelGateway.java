package br.com.cit.cc.pocs.model.marvel;

public interface MarvelGateway {

	MarvelResponse characters(MarvelRequest request);

}
