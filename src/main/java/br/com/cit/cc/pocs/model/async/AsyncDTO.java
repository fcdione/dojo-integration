package br.com.cit.cc.pocs.model.async;

import java.io.Serializable;

public class AsyncDTO implements Serializable {

	private static final long serialVersionUID = 6736143323095292448L;

	private final String to;

	private final String text;

	private final String subject;

	public AsyncDTO(final String to, final String text, final String subject) {
		this.to = to;
		this.text = text;
		this.subject = subject;
	}

	public String getTo() {
		return to;
	}

	public String getText() {
		return text;
	}

	public String getSubject() {
		return subject;
	}

}
