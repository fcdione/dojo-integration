package br.com.cit.cc.pocs.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@EnableAutoConfiguration
@ImportResource(locations = { "spring/spring-root.xml" })
public class SpringBoot {

	public static void main(final String[] args) throws Exception {
		SpringApplication.run(SpringBoot.class, args);
	}
}