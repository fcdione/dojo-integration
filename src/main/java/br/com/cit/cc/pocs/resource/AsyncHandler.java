package br.com.cit.cc.pocs.resource;

import org.slf4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

import br.com.cit.cc.pocs.model.async.AsyncDTO;

public class AsyncHandler {

	private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AsyncHandler.class);

	public void handleMessage(final Message<?> message) throws MessagingException, InterruptedException {
		if (AsyncDTO.class.isAssignableFrom(message.getPayload().getClass())) {

			for (int i = 0; i < 5; i++) {
				Thread.sleep(3600);
				LOGGER.info("async sender sleeping...");
			}

			final AsyncDTO asyncDTO = (AsyncDTO) message.getPayload();
			LOGGER.info("send fake email to:{} subject:{} text:{}", asyncDTO.getTo(), asyncDTO.getSubject(), asyncDTO.getText());
		} else {
			throw new RuntimeException("can't read this message");
		}

	}

}
