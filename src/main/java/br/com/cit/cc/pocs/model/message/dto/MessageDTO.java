package br.com.cit.cc.pocs.model.message.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MessageDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4005456474548363355L;

	private Long id;
	private String text;

	public MessageDTO() {
		super();
	}

	public MessageDTO(final Long id, final String message) {
		this.id = id;
		text = message;
	}

	public Long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("text", text).build();
	}

}
