package br.com.cit.cc.pocs.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.cit.cc.infrastructure.queue.QueueProducer;
import br.com.cit.cc.pocs.model.message.dto.MessageDTO;

@Controller
@RequestMapping("jms")
public class JmsController {

	@Autowired
	private QueueProducer jmsQueueProducer;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	String messsageInActiveMQ() {
		final long now = System.currentTimeMillis();
		final MessageDTO messageDTO = new MessageDTO(now, "test with id " + now);
		jmsQueueProducer.send(messageDTO);
		return String.format("sended message=%s", messageDTO);
	}

	@RequestMapping(value = "{quantity}", method = RequestMethod.GET)
	@ResponseBody
	String messsageInSQS(@PathVariable("quantity") final Long quantity) {
		long c;
		for (c = 0L; c < quantity; c++) {
			final long now = System.currentTimeMillis();
			jmsQueueProducer.send(new MessageDTO(now, "test " + now));
		}
		return String.format("sended %d messages", c);
	}

}
