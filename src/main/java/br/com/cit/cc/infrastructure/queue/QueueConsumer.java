package br.com.cit.cc.infrastructure.queue;

import java.util.Map;

public interface QueueConsumer extends QueueOperations {

	public void proccessQueue(final String payload, final Map<String, String> headers);

}