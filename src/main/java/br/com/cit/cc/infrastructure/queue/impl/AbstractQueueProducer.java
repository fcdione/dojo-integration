package br.com.cit.cc.infrastructure.queue.impl;

import java.util.Map;

import br.com.cit.cc.infrastructure.queue.QueueProducer;

public abstract class AbstractQueueProducer implements QueueProducer {

	protected final String queue;

	public AbstractQueueProducer(final String queue) {
		super();
		this.queue = queue;
	}

	protected <T> Map<String, Object> defaultHeaders(final T payload) {
		final Map<String, Object> headers = new java.util.HashMap<>();
		headers.put(MESSAGE_KEY, payload.getClass().getSimpleName());
		headers.put(QUEUE_ORIGIN, queue);
		return headers;
	}

}
