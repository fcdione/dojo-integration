package br.com.cit.cc.infrastructure.queue.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;

public class SqsQueueProducer extends AbstractQueueProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(SqsQueueProducer.class);

	private final QueueMessagingTemplate messagingTemplate;

	public SqsQueueProducer(final String queue, final QueueMessagingTemplate queueMessagingTemplate) {
		super(queue);
		messagingTemplate = queueMessagingTemplate;
	}

	@Override
	public <T> void send(final T payload) {
		final Map<String, Object> headers = defaultHeaders(payload);
		LOGGER.info("try send message with payload={} and headers={} on queue={}", payload, headers, queue);
		messagingTemplate.convertAndSend(queue, payload, headers);
	}



}
