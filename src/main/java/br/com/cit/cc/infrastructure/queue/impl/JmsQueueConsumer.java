package br.com.cit.cc.infrastructure.queue.impl;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.converter.MessageConversionException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.cit.cc.infrastructure.queue.QueueConsumer;
import br.com.cit.cc.infrastructure.queue.QueueProcessor;

public class JmsQueueConsumer implements QueueConsumer {

	private final QueueProcessor processor;

	private final static Logger LOGGER = LoggerFactory.getLogger(JmsQueueConsumer.class);

	public JmsQueueConsumer(final QueueProcessor processor) {
		this.processor = processor;
	}

	@Override
	public void proccessQueue(final String payload, final Map<String, String> headers) {
		LOGGER.info("receive message with payload={} and headers={} ", payload, headers);
		processor.process(payload, headers);
	}

	public void onMessage(final byte[] message) {
		try {
			final ExpectedMessage expectedMessage = new ObjectMapper().readValue(new String(message), ExpectedMessage.class);
			proccessQueue(expectedMessage.payload.toString(), expectedMessage.headers);
		} catch (final IOException e) {
			throw new MessageConversionException(String.format("unknown message=%s", new String(message)), e.getCause());
		}
	}

	private static class ExpectedMessage {
		@JsonProperty
		private JsonNode payload;
		@JsonProperty
		private Map<String, String> headers;
	}

}
