package br.com.cit.cc.infrastructure.queue.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import br.com.cit.cc.infrastructure.queue.QueueConsumer;
import br.com.cit.cc.infrastructure.queue.QueueProcessor;

public class SqsQueueConsumer implements QueueConsumer {

	private final static Logger LOGGER = LoggerFactory.getLogger(SqsQueueConsumer.class);

	private final QueueProcessor processor;

	public SqsQueueConsumer(final QueueProcessor processor) {
		super();
		this.processor = processor;
	}

	@Override
	@SqsListener(value = QUEUE, deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
	public void proccessQueue(@Payload final String payload, @Headers final Map<String, String> headers) {
		LOGGER.info("receive message with payload={} and headers={} ", payload, headers);
		processor.process(payload, headers);
	}

}
