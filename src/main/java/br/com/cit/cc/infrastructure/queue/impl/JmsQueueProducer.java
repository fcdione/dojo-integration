package br.com.cit.cc.infrastructure.queue.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsMessagingTemplate;

public class JmsQueueProducer extends AbstractQueueProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(JmsQueueProducer.class);

	private final JmsMessagingTemplate messagingTemplate;

	public JmsQueueProducer(final String queue, final JmsMessagingTemplate messagingTemplate) {
		super(queue);
		this.messagingTemplate = messagingTemplate;
	}

	@Override
	public <T> void send(final T payload) {
		final Map<String, Object> headers = defaultHeaders(payload);
		LOGGER.info("try send message with payload={} and headers={} on queue={}", payload, headers, queue);
		messagingTemplate.convertAndSend(queue, payload, headers);
	}

}
