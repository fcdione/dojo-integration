package br.com.cit.cc.infrastructure.queue;

interface QueueOperations {

	public final String QUEUE = "${queue.for.consumer}";
	public final String MESSAGE_KEY = "MESSAGE_KEY";
	public final String QUEUE_ORIGIN = "QUEUE_ORIGIN";

}
