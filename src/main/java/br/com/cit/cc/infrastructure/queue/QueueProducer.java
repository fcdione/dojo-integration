package br.com.cit.cc.infrastructure.queue;

public interface QueueProducer extends QueueOperations {

	<T> void send(T payload);

}