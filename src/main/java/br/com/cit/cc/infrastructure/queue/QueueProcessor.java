package br.com.cit.cc.infrastructure.queue;

import java.util.Map;

public interface QueueProcessor {

	void process(String payload, Map<String, String> headers);

}
