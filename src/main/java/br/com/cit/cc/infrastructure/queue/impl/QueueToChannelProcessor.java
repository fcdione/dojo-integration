package br.com.cit.cc.infrastructure.queue.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.converter.MessageConversionException;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.cit.cc.infrastructure.queue.QueueProcessor;

public class QueueToChannelProcessor implements QueueProcessor {

	private final static Logger LOGGER = LoggerFactory.getLogger(QueueToChannelProcessor.class);

	private final static String MESSAGE_KEY = "MESSAGE_KEY";
	private final MessageChannel channel;

	private Map<String, Class<?>> payloads = new HashMap<>();
	private String defaultPackage = getClass().getPackage().getName();

	public QueueToChannelProcessor(final MessageChannel channel) {
		this.channel = channel;
	}

	@Override
	public void process(final String payload, final Map<String, String> headers) {
		if (headers.containsKey(MESSAGE_KEY)) {
			try {
				convertAndPublish(payload, findSignatureOfMessage(payload, headers));
			} catch (final ClassNotFoundException e) {
				NotFoundPayloadSignatureException.create(payload, headers);
			}
		} else {
			NotFoundPayloadSignatureException.create(payload, headers);
		}
	}

	private <T> void convertAndPublish(final String payload, final Class<T> clazz) {
		try {
			LOGGER.info("try convert payload={} to object={}", payload, clazz);
			final T obj = new ObjectMapper().readValue(payload, clazz);
			channel.send(MessageBuilder.withPayload(obj).build());
		} catch (final IOException e) {
			throw new MessageConversionException(String.format("conversion exception message payload=%s in class=%s", payload, clazz.getName()), e.getCause());
		}
	}

	private Class<?> findSignatureOfMessage(final String payload, final Map<String, String> headers) throws ClassNotFoundException {
		final String key = headers.get(MESSAGE_KEY);
		return payloads.containsKey(key) ? payloads.get(key) : Class.forName(String.format("%s.%s", defaultPackage, key));
	}

	private static class NotFoundPayloadSignatureException extends MessageConversionException {

		private static final long serialVersionUID = 5374972384456947426L;

		private NotFoundPayloadSignatureException(final String description) {
			super(description);
		}

		static void create(final String payload, final Map<String, String> headers) {
			throw new NotFoundPayloadSignatureException(String.format("unrecognized signature from message with payload=%s and headers=%s", payload, headers));
		}

	}

	public void setDefaultPackage(final String defaultPackage) {
		this.defaultPackage = defaultPackage;
	}

	public void setPayloads(final Map<String, Class<?>> payloads) {
		this.payloads = payloads;
	}

}
